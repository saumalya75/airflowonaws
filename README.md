# AirflowOnAWS

### An Airflow Server hosted on ECS Fargate, using CeleryExecutor with Redis and Postgres.

## 1. Introduction:
__What is Airflow?__
[Airflow](https://airflow.apache.org/docs/stable/) is a data pipeline workflow scheduler tool. User can schedule numerous kinds of tasks and define inter dependency using Airflow. Airflow workflows and tasks are written in python. It comes under _**configuration as code**_ paradigm.  But I bet you already knew that, given that you are here. So let's talk about the project then!

This repository holds the required codes to build an iamge which will be used in setting up Airflow server on AWS ECS, with Fargate cluster. The airflow server uses CeleryExecutor to scale up the execution process. Redis is used as MessageBroker. One Postgres RDS instace is used as Airflow Meta-Database. For in-depth steps of setting up the server on ECS, please follow my blog: <TO DO>

## 2. Pre-requisite:
Not much, just a few things:

1. Install (if not installed already) latest version of [Docker](https://docs.docker.com).

2. Install (if not installed already) latest version of [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

## 3. Getting the application/code:
**Easy-peasy!**

The entire project is available on my BitBucket repository  **`https://bitbucket.org/saumalya75/airflowonaws/src/master/`**, which can be cloned using **`git clone git@bitbucket.org:saumalya75/airflowonaws.git`**.

## 4. Usage:
#### This code base is used to create the docker image required for AirflowOnAWS project, Please find the details document on the project (<TO DO>) or visit my blog: <TO DO>.
_Anyone can clone the code base and do there own modification on it to suit there personal need._

#### Following steps need to be followed to create the image:
1. Login to the machine where you want to launch the Airflow containers.
2. Install required softwares, follow pre-requisite section.
3. Clone master branch (always contains the latest stable version) from the previously mentioned repo.
4. Create `app/script/create-user.py` file by copying `app/script/create-user-template.py` file and provide intended airflow credentials. These credentials will be required to log into Airflow WebUI.
5. Execute relevant _`docker build`_ command provided in `build_command.txt` to build the image.
6. Follow Airflow server setup document: <TO DO>.

## 5. Conclusion:

Let me end the article with thanking you for going through the article. It will be highly appreciated if anyone have any suggestions, ideas to implement. In case of any queries, suggestion, I am available on _+91-9593090126_ and saumalya75@gmail.com.